var Discord = require("discord.js");
var command_dispatcher = require('./lib/commands-dispatcher');
var bot = new Discord.Client();

require('dotenv').config();

bot.login(process.env.APP_BOT_TOKEN);

bot.on('ready', function(){
  console.log('I am ready!');
  bot.on('message', function(message){
    var messageArray = message.content.split(' ');
    var firstWord = messageArray[0];
    var secondWord = messageArray[1];
    if(firstWord === "!Noodle" || firstWord === '!noodle'){
      command_dispatcher(messageArray, function(res){
        message.channel.sendMessage(res);
      });
    }
  });
});
