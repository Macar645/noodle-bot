/**
* Created by maximecaron on 2016-08-30.
*/
var winston = require('winston');
var request = require('request');

var league_url = "https://na.api.pvp.net/api/lol/na/v1.4/summoner/by-name/"

module.exports = function(message, callback) {
  var ping = function(callback) {
    callback("Pong!");
  };

  var summoner = function(callback, summoner_name){
    console.log(league_url + summoner_name + "?" + process.env.LEAGUE_API_KEY);
    request(league_url + summoner_name + "?" + process.env.LEAGUE_API_KEY, function (error, response, body) {
      if (!error && response.statusCode == 403) {
        data = body;
        callback(data);
      } else {
        console.log(error);
        callback("Error");
      }
    })
  };

  var quote = function(callback){
    request("http://quotesondesign.com/wp-json/posts?filter[orderby]=rand&filter[posts_per_page]=1", function (error, response, body) {
      if (!error && response.statusCode == 200) {
        data = JSON.parse(body)[0];
        quote = data.content.replace(/<(?:.|\n)*?>/gm, '');
        callback(data.title + ": " + quote);
      } else {
        callback(error);
        console.log(error);
      }
    })
  };

  switch(message[1].toLowerCase()){
    case("ping"):
      ping(callback);
      break;
    case("quote"):
      quote(callback);
      break;
    case("summoner"):
      var name = message[2];
      summoner(callback, name);
      break;
    default:
      callback("Invalid Command");
  }
};
